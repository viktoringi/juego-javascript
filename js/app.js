var cvs = document.getElementById("canvas");
var ctx = cvs.getContext("2d");

var nave = new Image();
var enemigo = new Image();
var fondo = new Image();
var balaNave = new Image();
var balaEnemigo = new Image();

nave.src = "imagenes/nave.png";
enemigo.src = "imagenes/enemigo.png";
fondo.src = "imagenes/fondo.png";
balaNave.src = "imagenes/balaNave.png";
balaEnemigo.src = "imagenes/balaEnemigo.png";

var audioDisparo = document.getElementById("audio");

var isGameOver=false;

var vida = 3;
var puntuacion = 0;

var naveX = 10;
var naveY = cvs.height/2-nave.height/2

var enemigos = [];
enemigos[0] = {
	x: cvs.width,
	y: Math.floor(Math.random()*335)+50
};

var balas = [];
var balasEnemigo = [];

// Permite controlar al jugador con las teclas 'WASD' y 'Ratón'
moverNave();
moverNaveRaton();

// Función para cuando el juego acabe
function gameOver() {

	isGameOver = true;
	
    document.getElementById('game-over').style.display = 'block';
    document.getElementById('game-over-overlay').style.display = 'block';
    document.getElementById('play-again').addEventListener('click', function() {

        location.reload();

    });
}

// Crea enemigos cada 1500ms
setInterval('createEnemy()',1500);

function startGame() {
	
	function draw() {

	// dibujo primero el fondo y luego la nave 
	ctx.drawImage(fondo,0,0);
	ctx.drawImage(nave, naveX, naveY);

	for (var i = 0; i < enemigos.length; i++){

		// dibuja los enemigos en el canvas y los mueve hacia la izquierda
		ctx.drawImage(enemigo,enemigos[i].x,enemigos[i].y);
		enemigos[i].x--;

		// elimina los enemigos cuando se salen del canvas
		if (enemigos[i].x<=0){
			vida--;
			if (vida==0) {
				gameOver();
			}
			enemigos.splice(i,1);
		}

		// detecta colisiones si la nave se solapa en los dos ejes con el enemigo
		if (naveX < enemigos[i].x + enemigo.width &&
			naveX + nave.width > enemigos[i].x &&
			naveY < enemigos[i].y + enemigo.height &&
			nave.height + naveY > enemigos[i].y){

			document.getElementById("collision").style.color = "red";
			document.getElementById("collision").value = "COLLISION DETECTED";
			enemigos.splice(i,1);
			vida --;
			if (vida<=0) {
				gameOver();
			}
		} else {
			document.getElementById("collision").style.color = "green";
			document.getElementById("collision").value = "NO COLLISION";
		}
	}

	// Dibuja las balas del jugador en el canvas
	for (var i = 0; i < balas.length; i++){

		ctx.drawImage(balaNave, balas[i].x, balas[i].y);

		balas[i].x+=4;

		if (balas[i].x>=cvs.width){
			balas.splice(i,1);
		}
			
	}

	ctx.font = "15px Arial";
	ctx.fillStyle = 'white';
	ctx.fillText("LEVEL: 1", cvs.width/2, 20);

	ctx.fillStyle = 'white';
	ctx.fillText("SCORE:  "+puntuacion, cvs.width-120, 20);

	ctx.fillStyle = 'white';
	ctx.fillText("HEALTH:  "+vida, cvs.width-260, 20);

	ctx.fillStyle = 'green';
	ctx.fillText("Enemigos actuales: "+enemigos.length, 20, 20);

	ctx.fillStyle = 'green';
	ctx.fillText("Balas actuales: "+balas.length, 200, 20);

	if (!isGameOver){
		requestAnimationFrame(draw);
		checkBulletsCollisions();
		}
}
	draw();
}

function checkBulletsCollisions() {

	for (var e = 0; e < enemigos.length; e++){

		for (var i = 0; i < balas.length; i++){

			if (balas.length<=enemigos.length){ // los disparos nunca pueden superar
				// la cantidad de enemigos, es decir bala x enemigo 
				if (balas[i].x < enemigos[e].x + enemigo.width &&
				balas[i].x + balaNave.width > enemigos[e].x &&
				balas[i].y < enemigos[e].y + enemigo.height &&
				balaNave.height + balas[i].y > enemigos[e].y){

				balas.splice(i,1);
				enemigos.splice(e,1);
				puntuacion++;

				}
			} 
		}

	}

}

function createEnemy() {
	enemigos.push({
			x: cvs.width,
			y: Math.floor(Math.random()*335)+50
	});
}

// faltan implementar los niveles

// mover nave con el ratón, hay que solucionar que la nave no se salga del 
// ámbito del juego
function moverNaveRaton() {
	canvas.addEventListener("mousemove", function (evt) {
    var mousePos = getMousePos(cvs, evt);
    naveX = mousePos.x;
    naveY = mousePos.y;
	}, false);
}


//Get Mouse Position
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

// Mover nave con las teclas WASD
function moverNave(){
	document.addEventListener("keydown", function (event){
		switch(event.keyCode) {
    		case 87: // W
    			if (naveY>=50) {
        			naveY-=20;
				}
        		break;
    		case 65: // A
    			if (naveX>=20) {
					naveX-=20;
    			}
        		break;
        	case 68: // D
        		if (naveX<=cvs.width-nave.width-40){
        			naveX+=20;
        		}
        		break;
        	case 83: // S
        		if (naveY<=cvs.height-nave.height-40) {
        			naveY+=20;
        		}
        		break;
        	case 32: // SPACE
        		balas.push({
        			x: naveX+nave.width+5,
					y: naveY+nave.height/2-5
				});
				audioDisparo.play();
        		break;
		}
	});
}